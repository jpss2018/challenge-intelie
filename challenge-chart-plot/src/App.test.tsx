import { mountPlots } from './App';
import { Plot } from './interfaces/Plot';
import { inputTest, plotsTest } from './TestUtils';

test('Building array of plots', () => {
    const plots: Plot[] = mountPlots(inputTest);
    expect(plots).toEqual(plotsTest);
});
