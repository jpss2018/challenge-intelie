import { Plot } from "./interfaces/Plot";

export const inputTest: string = `{ "type": "start", "timestamp": 1519862400000, "select": ["min_response_time", "max_response_time"], "group": ["os", "browser"] }
    { "type": "span", "timestamp": 1519862400000, "begin": 1519862400000, "end": 1519862460000 }
    { "type": "data", "timestamp": 1519862400000, "os": "linux", "browser": "chrome", "min_response_time": 0.1, "max_response_time": 1.3 }
    { "type": "data", "timestamp": 1519862400000, "os": "mac", "browser": "chrome", "min_response_time": 0.2, "max_response_time": 1.2 }
    { "type": "data", "timestamp": 1519862400000, "os": "mac", "browser": "firefox", "min_response_time": 0.3, "max_response_time": 1.2 }
    { "type": "data", "timestamp": 1519862400000, "os": "linux", "browser": "firefox", "min_response_time": 0.1, "max_response_time": 1.0 }
    { "type": "data", "timestamp": 1519862460000, "os": "linux", "browser": "chrome", "min_response_time": 0.2, "max_response_time": 0.9 }
    { "type": "data", "timestamp": 1519862460000, "os": "mac", "browser": "chrome", "min_response_time": 0.1, "max_response_time": 1.0 }
    { "type": "data", "timestamp": 1519862460000, "os": "mac", "browser": "firefox", "min_response_time": 0.2, "max_response_time": 1.1 }
    { "type": "data", "timestamp": 1519862460000, "os": "linux", "browser": "firefox", "min_response_time": 0.3, "max_response_time": 1.4 }
    { "type": "stop", "timestamp": 1519862460000 }`.replace(/ /g, '');

export const inputTestStop: string = `{ "type": "start", "timestamp": 1519862400000, "select": ["min_response_time", "max_response_time"], "group": ["os", "browser"]}
{ "type": "span", "timestamp": 1519862400000, "begin": 1519862400000, "end": 1519862460000}
{ "type": "data", "timestamp": 1519862400000, "os": "linux", "browser": "chrome", "min_response_time": 0.1, "max_response_time": 1.3 }
{ "type": "data", "timestamp": 1519862400000, "os": "mac", "browser": "chrome", "min_response_time": 0.2, "max_response_time": 1.2 }
{ "type": "stop", "timestamp": 1519862460000}
{ "type": "data", "timestamp": 1519862400000, "os": "mac", "browser": "firefox", "min_response_time": 0.3, "max_response_time": 1.2 }
{ "type": "data", "timestamp": 1519862400000, "os": "linux", "browser": "firefox", "min_response_time": 0.1, "max_response_time": 1.0 }
{ "type": "data", "timestamp": 1519862460000, "os": "linux", "browser": "chrome", "min_response_time": 0.2, "max_response_time": 0.9 }
{ "type": "data", "timestamp": 1519862460000, "os": "mac", "browser": "chrome", "min_response_time": 0.1, "max_response_time": 1.0 }
{ "type": "data", "timestamp": 1519862460000, "os": "mac", "browser": "firefox", "min_response_time": 0.2, "max_response_time": 1.1 }
{ "type": "data", "timestamp": 1519862460000, "os": "linux", "browser": "firefox", "min_response_time": 0.3, "max_response_time": 1.4 }
{ "type": "stop", "timestamp": 1519862460000}

`;

export const plotsTest: Plot[] = [
    { "type": "start", "timestamp": 1519862400000, "select": ["min_response_time", "max_response_time"], "group": ["os", "browser"] },
    { "type": "span", "timestamp": 1519862400000, "begin": 1519862400000, "end": 1519862460000 },
    { "type": "data", "timestamp": 1519862400000, "os": "linux", "browser": "chrome", "min_response_time": 0.1, "max_response_time": 1.3 },
    { "type": "data", "timestamp": 1519862400000, "os": "mac", "browser": "chrome", "min_response_time": 0.2, "max_response_time": 1.2 },
    { "type": "data", "timestamp": 1519862400000, "os": "mac", "browser": "firefox", "min_response_time": 0.3, "max_response_time": 1.2 },
    { "type": "data", "timestamp": 1519862400000, "os": "linux", "browser": "firefox", "min_response_time": 0.1, "max_response_time": 1.0 },
    { "type": "data", "timestamp": 1519862460000, "os": "linux", "browser": "chrome", "min_response_time": 0.2, "max_response_time": 0.9 },
    { "type": "data", "timestamp": 1519862460000, "os": "mac", "browser": "chrome", "min_response_time": 0.1, "max_response_time": 1.0 },
    { "type": "data", "timestamp": 1519862460000, "os": "mac", "browser": "firefox", "min_response_time": 0.2, "max_response_time": 1.1 },
    { "type": "data", "timestamp": 1519862460000, "os": "linux", "browser": "firefox", "min_response_time": 0.3, "max_response_time": 1.4 },
    { "type": "stop", "timestamp": 1519862460000 }
];

export const mapTest: any = {
    data: {
        linuxchrome: [
            { type: "data", timestamp: 1519862400000, os: "linux", browser: "chrome", min_response_time: 0.1, max_response_time: 1.3 },
            { type: "data", timestamp: 1519862460000, os: "linux", browser: "chrome", min_response_time: 0.2, max_response_time: 0.9 }
        ],
        linuxfirefox: [
            { type: "data", timestamp: 1519862400000, os: "linux", browser: "firefox", min_response_time: 0.1, max_response_time: 1 },
            { type: "data", timestamp: 1519862460000, os: "linux", browser: "firefox", min_response_time: 0.3, max_response_time: 1.4 }
        ],
        macchrome: [
            { type: "data", timestamp: 1519862400000, os: "mac", browser: "chrome", min_response_time: 0.2, max_response_time: 1.2 },
            { type: "data", timestamp: 1519862460000, os: "mac", browser: "chrome", min_response_time: 0.1, max_response_time: 1 }
        ],
        macfirefox: [
            { type: "data", timestamp: 1519862400000, os: "mac", browser: "firefox", min_response_time: 0.3, max_response_time: 1.2 },
            { type: "data", timestamp: 1519862460000, os: "mac", browser: "firefox", min_response_time: 0.2, max_response_time: 1.1 }
        ]
    },
    span: { type: "span", timestamp: 1519862400000, begin: 1519862400000, end: 1519862460000 },
    start: { type: "start", timestamp: 1519862400000, select: ["min_response_time", "max_response_time"], group: ["os", "browser"] },
    stop: { type: "stop", timestamp: 1519862460000 },
}

export const mapTestStop: any = {
    "start": {
        "type": "start",
        "timestamp": 1519862400000,
        "select": [
            "min_response_time",
            "max_response_time"
        ],
        "group": [
            "os",
            "browser"
        ]
    },
    "span": {
        "type": "span",
        "timestamp": 1519862400000,
        "begin": 1519862400000,
        "end": 1519862460000
    },
    "data": {
        "linuxchrome": [
            {
                "type": "data",
                "timestamp": 1519862400000,
                "os": "linux",
                "browser": "chrome",
                "min_response_time": 0.1,
                "max_response_time": 1.3
            }
        ],
        "macchrome": [
            {
                "type": "data",
                "timestamp": 1519862400000,
                "os": "mac",
                "browser": "chrome",
                "min_response_time": 0.2,
                "max_response_time": 1.2
            }
        ]
    },
    "stop": {
        "type": "stop",
        "timestamp": 1519862460000
    }
}

export const mapTestStartAgain: any = {
    "start": {
        "type": "start",
        "timestamp": 1519862400000,
        "select": [
            "min_response_time",
            "max_response_time"
        ],
        "group": [
            "os",
            "browser"
        ]
    },
    "span": {
        "type": "span",
        "timestamp": 1519862400000,
        "begin": 1519862400000,
        "end": 1519862460000
    },
    "data": {
        "linuxchrome": [
            {
                "type": "data",
                "timestamp": 1519862400000,
                "os": "linux",
                "browser": "chrome",
                "min_response_time": 0.1,
                "max_response_time": 1.3
            }
        ]
    },
    "stop": {
        "type": "stop",
        "timestamp": 1519862460000
    }
};

export const seriesCreate = [
    {
        "key": "linuxchromemin_response_time",
        "payload": {
            "id": "linuxchromemin_response_time",
            "type": "circle",
            "color": "#6dba52",
            "value": "Linux Chrome Min Response Time"
        },
        "data": [
            {
                "category": "00:00",
                "value": 0.1
            },
            {
                "category": "01:00",
                "value": 0.2
            }
        ]
    },
    {
        "key": "linuxchromemax_response_time",
        "payload": {
            "id": "linuxchromemax_response_time",
            "type": "circle",
            "color": "#26715d",
            "value": "Linux Chrome Max Response Time"
        },
        "data": [
            {
                "category": "00:00",
                "value": 1.3
            },
            {
                "category": "01:00",
                "value": 0.9
            }
        ]
    },
    {
        "key": "linuxfirefoxmin_response_time",
        "payload": {
            "id": "linuxfirefoxmin_response_time",
            "type": "circle",
            "color": "#7ebfdf",
            "value": "Linux Firefox Min Response Time"
        },
        "data": [
            {
                "category": "00:00",
                "value": 0.1
            },
            {
                "category": "01:00",
                "value": 0.3
            }
        ]
    },
    {
        "key": "linuxfirefoxmax_response_time",
        "payload": {
            "id": "linuxfirefoxmax_response_time",
            "type": "circle",
            "color": "#3866b1",
            "value": "Linux Firefox Max Response Time"
        },
        "data": [
            {
                "category": "00:00",
                "value": 1
            },
            {
                "category": "01:00",
                "value": 1.4
            }
        ]
    },
    {
        "key": "macchromemin_response_time",
        "payload": {
            "id": "macchromemin_response_time",
            "type": "circle",
            "color": "#8768a3",
            "value": "Mac Chrome Min Response Time"
        },
        "data": [
            {
                "category": "00:00",
                "value": 0.2
            },
            {
                "category": "01:00",
                "value": 0.1
            }
        ]
    },
    {
        "key": "macchromemax_response_time",
        "payload": {
            "id": "macchromemax_response_time",
            "type": "circle",
            "color": "#544669",
            "value": "Mac Chrome Max Response Time"
        },
        "data": [
            {
                "category": "00:00",
                "value": 1.2
            },
            {
                "category": "01:00",
                "value": 1
            }
        ]
    },
    {
        "key": "macfirefoxmin_response_time",
        "payload": {
            "id": "macfirefoxmin_response_time",
            "type": "circle",
            "color": "#f8cc2d",
            "value": "Mac Firefox Min Response Time"
        },
        "data": [
            {
                "category": "00:00",
                "value": 0.3
            },
            {
                "category": "01:00",
                "value": 0.2
            }
        ]
    },
    {
        "key": "macfirefoxmax_response_time",
        "payload": {
            "id": "macfirefoxmax_response_time",
            "type": "circle",
            "color": "#e9a854",
            "value": "Mac Firefox Max Response Time"
        },
        "data": [
            {
                "category": "00:00",
                "value": 1.2
            },
            {
                "category": "01:00",
                "value": 1.1
            }
        ]
    }
]

export const seriesStop = [
    {
        "key": "linuxchromemin_response_time",
        "payload": {
            "id": "linuxchromemin_response_time",
            "type": "circle",
            "color": "#6dba52",
            "value": "Linux Chrome Min Response Time"
        },
        "data": [
            {
                "category": "00:00",
                "value": 0.1
            }
        ]
    },
    {
        "key": "linuxchromemax_response_time",
        "payload": {
            "id": "linuxchromemax_response_time",
            "type": "circle",
            "color": "#26715d",
            "value": "Linux Chrome Max Response Time"
        },
        "data": [
            {
                "category": "00:00",
                "value": 1.3
            }
        ]
    },
    {
        "key": "macchromemin_response_time",
        "payload": {
            "id": "macchromemin_response_time",
            "type": "circle",
            "color": "#8768a3",
            "value": "Mac Chrome Min Response Time"
        },
        "data": [
            {
                "category": "00:00",
                "value": 0.2
            }
        ]
    },
    {
        "key": "macchromemax_response_time",
        "payload": {
            "id": "macchromemax_response_time",
            "type": "circle",
            "color": "#544669",
            "value": "Mac Chrome Max Response Time"
        },
        "data": [
            {
                "category": "00:00",
                "value": 1.2
            }
        ]
    }
]

export const seriesRestart = [
    {
        "key": "linuxchromemin_response_time",
        "payload": {
            "id": "linuxchromemin_response_time",
            "type": "circle",
            "color": "#6dba52",
            "value": "Linux Chrome Min Response Time"
        },
        "data": [
            {
                "category": "00:00",
                "value": 0.1
            }
        ]
    },
    {
        "key": "linuxchromemax_response_time",
        "payload": {
            "id": "linuxchromemax_response_time",
            "type": "circle",
            "color": "#26715d",
            "value": "Linux Chrome Max Response Time"
        },
        "data": [
            {
                "category": "00:00",
                "value": 1.3
            }
        ]
    }
]

export const objectsEqual = (o1: any, o2: any): boolean => {
    return typeof o1 === 'object' && Object.keys(o1).length > 0
        ? Object.keys(o1).length === Object.keys(o2).length
        && Object.keys(o1).every(p => objectsEqual(o1[p], o2[p]))
        : o1 === o2;
}

export const arraysEqual = (a1: any[], a2: any[]) => a1.length === a2.length && a1.every((o, idx) => objectsEqual(o, a2[idx]));