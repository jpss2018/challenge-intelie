export interface Plot {
    type: string;
    timestamp: number;
    select?: string[];
    group?: string[];
    os?: string;
    browser?: string;
    min_response_time?: number;
    max_response_time?: number;
    begin?: number;
    end?: number;
}
