import React from 'react';
import { Button } from 'react-bootstrap';

export default (props: any) => {

    return (
        <div className="footer">
            <Button variant="primary" onClick={props.click}>{props.btnName}</Button>
        </div>
    )
};
