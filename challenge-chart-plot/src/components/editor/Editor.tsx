import AceEditor from "react-ace";

import "ace-builds/src-noconflict/mode-java";
import "ace-builds/src-noconflict/theme-monokai";
import "ace-builds/src-noconflict/ext-language_tools";
import '../../assets/scss/editor.scss';

export default (props: any) => {

    return (

        <AceEditor
            height='300px'
            width="100%"
            mode="javascript"
            theme="monokai"
            name="blah2"
            className="container-editor"
            onChange={props.onChange}
            fontSize={14}
            showPrintMargin={false}
            showGutter={true}
            highlightActiveLine={false}
            value={props.value}
            setOptions={{
                enableBasicAutocompletion: false,
                enableLiveAutocompletion: false,
                enableSnippets: false,
                showLineNumbers: true,
                tabSize: 2,
            }} />

    );
};
