import { mountPlots } from '../../App';
import { mountMap, mountSeries } from '../../components/line-chart/LineChartCustom';
import { mapTest, mapTestStop, plotsTest, mapTestStartAgain, seriesCreate, seriesStop, seriesRestart } from '../../TestUtils';

test('Check if map is create correctlly', () => {
    const map = mountMap(plotsTest);
    expect(map).toEqual(mapTest);
});

test('Check if series is create correctlly', () => {
    const series = mountSeries(mapTest);
    expect(series).toEqual(seriesCreate);
});

test('Check if series with stop is create correctlly', () => {
    const series = mountSeries(mapTestStop);
    expect(series).toEqual(seriesStop);
});

test('Check if series with start again is create correctlly', () => {
    const series = mountSeries(mapTestStartAgain);
    expect(series).toEqual(seriesRestart);
});
