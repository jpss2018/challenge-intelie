import { LineChart, Line, XAxis, Tooltip, CartesianGrid, Legend, ResponsiveContainer } from 'recharts';
import { Plot } from '../../interfaces/Plot';
import '../../assets/scss/chart.scss';
import moment from 'moment';

export interface Props {
    plots: Plot[];
}

const PAYLOAD = [
    { id: 'linuxchromemin_response_time', type: 'circle', color: '#6dba52', value: 'Linux Chrome Min Response Time' },
    { id: 'linuxchromemax_response_time', type: 'circle', color: '#26715d', value: 'Linux Chrome Max Response Time' },
    { id: 'macchromemin_response_time', type: 'circle', color: '#8768a3', value: 'Mac Chrome Min Response Time' },
    { id: 'macchromemax_response_time', type: 'circle', color: '#544669', value: 'Mac Chrome Max Response Time' },
    { id: 'linuxfirefoxmin_response_time', type: 'circle', color: '#7ebfdf', value: 'Linux Firefox Min Response Time' },
    { id: 'linuxfirefoxmax_response_time', type: 'circle', color: '#3866b1', value: 'Linux Firefox Max Response Time' },
    { id: 'macfirefoxmin_response_time', type: 'circle', color: '#f8cc2d', value: 'Mac Firefox Min Response Time' },
    { id: 'macfirefoxmax_response_time', type: 'circle', color: '#e9a854', value: 'Mac Firefox Max Response Time' }
];

export const mountSeries = (map: any) => {

    let series: any[] = [];
    let mapData: any = map.hasOwnProperty("data") ? map["data"] : {};
    let selects: string[] = map.hasOwnProperty("start") ? map["start"].select : [];
    let span = map.hasOwnProperty("span") ? map["span"] : {};
    let { begin, end } = span;

    Object.keys(mapData).forEach((key: string) => {
        selects.forEach((select: any) => {

            let keyAux = key + select;
            let payload: any = PAYLOAD.find(ele => ele.id === keyAux);
            let data: any = [];

            mapData[key].forEach((ele: any) => {

                if (ele.timestamp >= begin && ele.timestamp <= end) {
                    let timestamp = moment(new Date(ele.timestamp)).format('mm:ss');
                    let obj: any = { category: timestamp, value: ele[select] };

                    data.push(obj);
                }
            });

            if (payload) {
                let serie: any = { key: keyAux, payload, data };
                series.push(serie);
            }
        });
    });

    return series;
}

export const mountMap = (plots: Plot[]) => {

    const map: any = {};
    let mapData: any = {};
    let restart = false;

    plots.forEach((plot: any) => {

        map[plot.type] = plot.type !== "data" ? plot : {};

        if (plot.type === "start") {
            mapData = {};
            restart = true;
        }

        if (plot.type === "stop") {
            restart = false;
            return;
        }

        if (restart && plot.type === "data") {
            let { group } = map["start"];
            let keyAux = "";

            for (const key in plot) {
                if (group) {
                    group.forEach((ele: any) => {
                        if (ele === key) {
                            keyAux += plot[key];
                        }
                    });
                }
            }

            if (!mapData[keyAux]) {
                mapData[keyAux] = [];
            }

            mapData[keyAux].push(plot);
        }
    });

    if (map.hasOwnProperty("data")) {
        map["data"] = mapData;
    }

    return map;
}

const renderColorfulLegendText = (value: any, entry: any) => {
    const { color } = entry;
    return <span style={{ color }}>{value}</span>;
}

export default (props: any) => {
    let { plots } = props || [];

    let map = mountMap(plots);
    let series = mountSeries(map);
    let span = map.hasOwnProperty("span") ? map["span"] : {};
    let { begin, end } = span;

    return (
        <ResponsiveContainer className="container-chart">

            <LineChart data={series} >
                <CartesianGrid vertical={false} />
                <Legend
                    wrapperStyle={{ padding: "0 35px" }} align="right" layout="vertical"
                    verticalAlign="top" iconType="circle" formatter={renderColorfulLegendText}
                />

                <XAxis
                    dataKey="category" type="category" allowDuplicatedCategory={false}
                    tickSize={10} interval="preserveStart" tick={begin !== undefined && end !== undefined}
                />

                {series.map(s =>
                (
                    <Line dataKey="value" data={s.data} name={s.payload.value}
                        key={s.payload.id} stroke={s.payload.color} fill={s.payload.color}
                        dot={{ stroke: s.payload.color, strokeWidth: 5, }} />
                )
                )
                }

                <Tooltip isAnimationActive={false}></Tooltip>
            </LineChart>
        </ResponsiveContainer>
    )
};
