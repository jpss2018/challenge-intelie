import React, { Component } from 'react';
import './assets/scss/app.scss';
import './assets/scss/scrollbar.scss';
import Header from './components/header/Header';
import Editor from './components/editor/Editor';
import Footer from './components/footer/Footer';
import LineChartCustom from './components/line-chart/LineChartCustom';
import { Plot } from './interfaces/Plot';
import { inputTest } from './TestUtils';

export const mountPlots = (value: string) => {
    let plots: Plot[] = [];
    if (value !== undefined) {
        let objs: any = value
            .replace(/ /g, '')
            .replace(/\r/g, '')
            .split('\n');


        objs = objs.filter((elem: any) => elem !== '');
        objs.forEach((elem: any) => plots.push(JSON.parse(elem)));
    }

    return plots;
}

export default class App extends Component {

    state = {
        value: inputTest,
        plots: mountPlots(inputTest)
    }

    onChange = (newValue: any) => {
        this.setState({ value: newValue });
    }

    generateChart = () => {
        let plots: Plot[] = mountPlots(this.state.value);
        this.setState({ plots });
    }

    render() {
        return (
            <div id="app">
                <Header title="João Paulo Santos da Silva's Challenge" />
                <div className="content-app scrollbar">
                    <Editor value={this.state.value} onChange={this.onChange}> </Editor>

                    <div className="chart">
                        <LineChartCustom plots={this.state.plots} />
                    </div>
                </div>

                <Footer click={this.generateChart} btnName='GENERATE CHART' />
            </div>
        );
    }
};
