# Challenge Intelie

In this project is resolve of the intelie challenges: challenge-eventstore and chalenge-chart-plot

# Challenge Event Store

To resolve this challenge I created 2 classes "EventStoreImp, EventIteratorImp" that are responsible for implements the interfaces of the challenge.

Below is evidence of the thread-safety that can be seen on lines 14 and 19 of the EventStoreImp class:

```java
public EventStoreImp() {
   // Create a synchronized list
   this.events = Collections.synchronizedList(new ArrayList<Event>());
}

@Override
public void insert(Event event) {
  synchronized (this.events) {
     // must be in synchronized block
     this.events.add(event);
  }
}
```


# Challenge Event Store

In the project directory, you can run:

```npm start```

Runs the app in the development mode.

Open http://localhost:3000 to view it in the browser.

The page will reload if you make edits.

```npm test```

Launches the test runner in the interactive watch mode.
