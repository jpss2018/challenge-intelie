package net.intelie.challenges;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class EventStoreImp implements EventStore{
	
	private List<Event> events;
	
	public EventStoreImp() {
		 // Create a synchronized list
		 this.events = Collections.synchronizedList(new ArrayList<Event>());
	}
	
	@Override
	public void insert(Event event) {
		synchronized (this.events) {
			// must be in synchronized block
			this.events.add(event);
		}
	}

	@Override
	public void removeAll(String type) {
		synchronized (this.events) {
			// must be in synchronized block
			EventIterator eventIterator = new EventIteratorImp(events);
			    
		    while(eventIterator.moveNext()) {	    	
		    	Event event = eventIterator.current();
		    	
		    	if(type.equals(event.type())) {
					eventIterator.remove();
				}
		    }
		}
	}

	@Override
	public EventIterator query(String type, long startTime, long endTime) {
		
		synchronized (this.events) {
			// must be in synchronized block
			
			List<Event> events = this.events
					.stream()
					.filter(event -> type.equals(event.type()) && event.timestamp() >= startTime && event.timestamp() < endTime)
					.collect(Collectors.toList());
			
			EventIterator eventIterator = new EventIteratorImp(events);
			
			return eventIterator;
		}
	}
	
	public List<Event> getEvents() {
		return this.events;
	}
}
