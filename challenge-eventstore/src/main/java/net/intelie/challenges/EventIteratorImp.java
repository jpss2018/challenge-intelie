package net.intelie.challenges;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class EventIteratorImp implements EventIterator{
	
	private List<Event> events;
	private Iterator<Event> iterator;
	
	public EventIteratorImp(List<Event> events) {
		this.events = Collections.synchronizedList(events);
		
		// must be in synchronized block
		synchronized (this.events) {
			iterator = events.iterator();
		}
	}

	@Override
	public void close() throws Exception {		
	}

	@Override
	public boolean moveNext() {	
		// must be in synchronized block
		synchronized (this.events) {
			return this.iterator.hasNext();
		}
	}

	@Override
	public Event current() {
		// must be in synchronized block
		synchronized (this.events) {			
			return iterator.next();
		}
	}

	@Override
	public void remove() {
		// must be in synchronized block
		synchronized (this.events) {			
			this.iterator.remove();
		}
	}
}
