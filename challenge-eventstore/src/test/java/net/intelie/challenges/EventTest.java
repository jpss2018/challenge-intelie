package net.intelie.challenges;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class EventTest {
    @Test
    public void thisIsAWarning() throws Exception {
        Event event = new Event("some_type", 123L);

        //THIS IS A WARNING:
        //Some of us (not everyone) are coverage freaks.
        assertEquals(123L, event.timestamp());
        assertEquals("some_type", event.type());
    }
    
    @Test
    public void insert() throws Exception {
        
    	final int length = 10;
        
    	EventStoreImp eventStoreImpl = new EventStoreImp();
        
        for(int i = 1; i<=length; i++) {
        	Event event = new Event("Type #" + i, i);
        	eventStoreImpl.insert(event);
        }
        
        EventIterator eventIterator = new EventIteratorImp(eventStoreImpl.getEvents());
        
        List<Event> mock = new ArrayList<Event>();
        
        for(int i = 1; i<=length; i++) {
        	mock.add(new Event("Type #" + i, i));
        }
        
        assertEquals(compareEvents(eventIterator, mock), true);      
    }
    
    @Test
    public void removeAll() throws Exception {
        
    	final int length = 10;
        
    	EventStoreImp eventStoreImpl = new EventStoreImp();
        
        for(int i = 1; i<=length; i++) {
        	Event event = new Event("Type #" + i, i);
        	eventStoreImpl.insert(event);
        }
        
        eventStoreImpl.removeAll("Type #2");
        
        EventIterator eventIterator = new EventIteratorImp(eventStoreImpl.getEvents());
        
        List<Event> mock = new ArrayList<Event>();
        
        for(int i = 1; i<=length; i++) {
        	if(i != 2) {
        		mock.add(new Event("Type #" + i, i));
        	}
        }
        
        assertEquals(compareEvents(eventIterator, mock), true);      
    }
    
    @Test
    public void query() throws Exception {
        
        EventStoreImp eventStoreImpl = new EventStoreImp();
        
        for(int i = 0; i<100; i = i + 10) {
        	Event event = new Event("Type #1", i);        	
        	eventStoreImpl.insert(event);
        }
        
        for(int i = 0; i<25; i = i + 5) {
        	Event event = new Event("Type #2", i);
        	eventStoreImpl.insert(event);
        }
        
        for(int i = 3; i<10; i++) {
        	Event event = new Event("Type #" + i, i);
        	eventStoreImpl.insert(event);
        }
        
        
        List<Event> mock = new ArrayList<Event>();
        mock.add(new Event("Type #2", 5));
        mock.add(new Event("Type #2", 10));
        mock.add(new Event("Type #2", 15));
        
        EventIterator eventIterator =  eventStoreImpl.query("Type #2", 5, 20);

        assertEquals(compareEvents(eventIterator, mock), true);        
    }
    
    private boolean compareEvents(EventIterator eventIterator, List<Event> events) {
    	
    	List<Event> eventsAux = new ArrayList<Event>();
    	         
         while(eventIterator.moveNext()) {
         	Event eventA = eventIterator.current();        	
         	eventsAux.add(eventA);        	
         }
    	
    	return events.equals(eventsAux);
    }
    

}